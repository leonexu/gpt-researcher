import json


s = '''
{
    "results": [
        {
            "title": "Language Agent Tree Search Unifies Reasoning Acting and Planning in Language Models",
            "id": "2310.04406v3",
            "summary": "While language models (LMs) have shown potential across a range of decision-making tasks, their reliance on simple acting processes limits their broad deployment as autonomous agents. In this paper, we introduce Language Agent Tree Search (LATS) -- the first general framework that synergizes the capabilities of LMs in reasoning, acting, and planning. By leveraging the in-context learning ability of LMs, we integrate Monte Carlo Tree Search into LATS to enable LMs as agents, along with LM-powered value functions and self-reflections for proficient exploration and enhanced decision-making. A key feature of our approach is the incorporation of an environment for external feedback, which offers a more deliberate and adaptive problem-solving mechanism that surpasses the constraints of existing techniques. Our experimental evaluation across diverse domains validates the effectiveness and generality of LATS in decision-making while maintaining competitive or improved reasoning performance.",
            "pdf_url": "http://arxiv.org/pdf/2310.04406v3",
            "links": [
                "http://arxiv.org/abs/2310.04406v3",
                "http://arxiv.org/pdf/2310.04406v3"
            ],
            "reasoning": "Chosen for introducing a novel framework, Language Agent Tree Search, that integrates language models in reasoning, acting, and planning, showcasing its effectiveness in decision-making across diverse domains."
        },
        {
            "title": "When is Tree Search Useful for LLM Planning? It Depends on the Discriminator",
            "id": "2402.10890v2",
            "summary": "In this paper, we examine how large language models (LLMs) solve multi-step problems under a language agent framework with three components: a generator, a discriminator, and a planning method. We investigate the practical utility of two advanced planning methods, iterative correction and tree search. We present a comprehensive analysis of how discrimination accuracy affects the overall performance of agents when using these methods or a simpler method, re-ranking. Experiments on two tasks demonstrate that advanced planning methods demand discriminators with at least 90% accuracy to achieve significant improvements over re-ranking.",
            "pdf_url": "http://arxiv.org/pdf/2402.10890v2",
            "links": [
                "http://arxiv.org/abs/2402.10890v2",
                "http://arxiv.org/pdf/2402.10890v2"
            ],
            "reasoning": "Selected for analyzing the effectiveness of tree search in planning for large language models and the impact of discriminator accuracy on performance, providing valuable insights into planning methods."
        },
        {
            "title": "Natural Language Processing using Hadoop and KOSHIK",
            "id": "1608.04434v1",
            "summary": "Natural language processing, as a data analytics related technology, is used widely in many research areas such as artificial intelligence, human language processing, and translation. This study describes how to build a KOSHIK platform with relevant tools to analyze wiki data using Hadoop, evaluating the advantages and disadvantages of the KOSHIK architecture for processing performance.",
            "pdf_url": "http://arxiv.org/pdf/1608.04434v1",
            "links": [
                "http://arxiv.org/abs/1608.04434v1",
                "http://arxiv.org/pdf/1608.04434v1"
            ],
            "reasoning": "Included for its practical approach to utilizing Hadoop for natural language processing and discussing the architecture of KOSHIK for effective data analysis."
        },
        {
            "title": "Integrating AI Planning with Natural Language Processing: A Combination of Explicit and Tacit Knowledge",
            "id": "2202.07138v2",
            "summary": "This paper outlines the connections between AI planning and natural language processing, emphasizing the impact of each on the other in areas such as planning-based text understanding, explainability, and human-robot interaction. It explores the potential future issues and highlights the deep connections between AI planning and natural language processing.",
            "pdf_url": "http://arxiv.org/pdf/2202.07138v2",
            "links": [
                "http://arxiv.org/abs/2202.07138v2",
                "http://arxiv.org/pdf/2202.07138v2"
            ],
            "reasoning": "Chosen for its comprehensive overview of the relationship between AI planning and natural language processing, offering insights into leveraging both fields for improved communication and interaction."
        }
    ]
}
{
    "results": [
        {
            "title": "Language Agent Tree Search Unifies Reasoning Acting and Planning in Language Models",
            "id": "2310.04406v3",
            "summary": "While language models (LMs) have shown potential across a range of decision-making tasks, their reliance on simple acting processes limits their broad deployment as autonomous agents. In this paper, we introduce Language Agent Tree Search (LATS) -- the first general framework that synergizes the capabilities of LMs in reasoning, acting, and planning. By leveraging the in-context learning ability of LMs, we integrate Monte Carlo Tree Search into LATS to enable LMs as agents, along with LM-powered value functions and self-reflections for proficient exploration and enhanced decision-making. A key feature of our approach is the incorporation of an environment for external feedback, which offers a more deliberate and adaptive problem-solving mechanism that surpasses the constraints of existing techniques. Our experimental evaluation across diverse domains validates the effectiveness and generality of LATS in decision-making while maintaining competitive or improved reasoning performance.",
            "pdf_url": "http://arxiv.org/pdf/2310.04406v3",
            "links": [
                "http://arxiv.org/abs/2310.04406v3",
                "http://arxiv.org/pdf/2310.04406v3"
            ],
            "reasoning": "Chosen for introducing a novel framework, Language Agent Tree Search, that integrates language models in reasoning, acting, and planning, showcasing its effectiveness in decision-making across diverse domains."
        },
        {
            "title": "When is Tree Search Useful for LLM Planning? It Depends on the Discriminator",
            "id": "2402.10890v2",
            "summary": "In this paper, we examine how large language models (LLMs) solve multi-step problems under a language agent framework with three components: a generator, a discriminator, and a planning method. We investigate the practical utility of two advanced planning methods, iterative correction and tree search. We present a comprehensive analysis of how discrimination accuracy affects the overall performance of agents when using these methods or a simpler method, re-ranking. Experiments on two tasks demonstrate that advanced planning methods demand discriminators with at least 90% accuracy to achieve significant improvements over re-ranking.",
            "pdf_url": "http://arxiv.org/pdf/2402.10890v2",
            "links": [
                "http://arxiv.org/abs/2402.10890v2",
                "http://arxiv.org/pdf/2402.10890v2"
            ],
            "reasoning": "Selected for analyzing the effectiveness of tree search in planning for large language models and the impact of discriminator accuracy on performance, providing valuable insights into planning methods."
        },
        {
            "title": "Natural Language Processing using Hadoop and KOSHIK",
            "id": "1608.04434v1",
            "summary": "Natural language processing, as a data analytics related technology, is used widely in many research areas such as artificial intelligence, human language processing, and translation. This study describes how to build a KOSHIK platform with relevant tools to analyze wiki data using Hadoop, evaluating the advantages and disadvantages of the KOSHIK architecture for processing performance.",
            "pdf_url": "http://arxiv.org/pdf/1608.04434v1",
            "links": [
                "http://arxiv.org/abs/1608.04434v1",
                "http://arxiv.org/pdf/1608.04434v1"
            ],
            "reasoning": "Included for its practical approach to utilizing Hadoop for natural language processing and discussing the architecture of KOSHIK for effective data analysis."
        },
        {
            "title": "Integrating AI Planning with Natural Language Processing: A Combination of Explicit and Tacit Knowledge",
            "id": "2202.07138v2",
            "summary": "This paper outlines the connections between AI planning and natural language processing, emphasizing the impact of each on the other in areas such as planning-based text understanding, explainability, and human-robot interaction. It explores the potential future issues and highlights the deep connections between AI planning and natural language processing.",
            "pdf_url": "http://arxiv.org/pdf/2202.07138v2",
            "links": [
                "http://arxiv.org/abs/2202.07138v2",
                "http://arxiv.org/pdf/2202.07138v2"
            ],
            "reasoning": "Chosen for its comprehensive overview of the relationship between AI planning and natural language processing, offering insights into leveraging both fields for improved communication and interaction."
        }
    ]
}

'''

json_strings = s.replace("\n","").split('}{')

for idx, json_str in enumerate(json_strings):
    if idx != len(json_strings)-1: 
        x = json.loads(json_str + '}')    
        print(x)
    else:
        x = json.loads('{'+json_str)
        print(x)
