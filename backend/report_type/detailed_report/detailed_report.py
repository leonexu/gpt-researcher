import asyncio
import os
from fastapi import WebSocket

from gpt_researcher.master.agent import GPTResearcher
from gpt_researcher.master.functions import (add_source_urls, extract_headers,
                                             table_of_contents)


# task_description = '''
# 研究背景:测试是编译器等复杂软件质量保障的主要手段。但复杂软件的测试目前还面临-系列的关键挑战，如测试覆盖率不足，揭错能力较弱，严重依赖人工判定结果等。大模型能够理解代码的深层次的业务逻辑，研究基于大模型的复杂软件测试技术，有望大幅提高复杂软件测试水平，提升软件质量保障能力。
# 研究目标:研究提出基于大模型的大批量、自动化、高覆盖的测试代码生成方法，利用大模型对软件代码以及测试代码的理解能力，实现测试输入的自动生成与选择，进而实现测试代码的自动生成，解决现有测试代码生成技术无法准确预测参数范围的问题。研究提出基于AI的测试排序以及准确、快速的测试验证技术。利用基于深度神经网络的测试预言生成技术，解决现有测试技术难以自动验证测试结果的问题，提高软件测试的揭错率，实现智能化的测试代码生成、测试排序与测试验证，提高软件测试的效率和效果。
# '''

task_description = """
# Research background: 
Testing is the main means of quality assurance for complex software such as compilers. However, the testing of complex software still faces a series of key challenges, such as insufficient test coverage, weak error detection ability, and heavy reliance on manual judgment results. Large language models (LLMs) can understand the deep business logic of the code, and LLMs-based complex software testing technology is expected to greatly improve software testing qulity.

# Research objective: 
To research and propose a method for the automatic generation of large-scale, automated, and highly-covered test code based on LLMs. By leveraging the LLMs' ability to understand software code and test code, achieve automatic generation and selection of test inputs, thus enabling automatic generation of test code. Address the issues of existing test code generation techniques - unable to accurately predict parameter ranges. Also propose AI-based test prioritization and accurate, rapid test verification techniques. By utilizing test oracle generation techniques based on deep neural networks, solve the problem of existing testing techniques being unable to automatically verify test results, thereby improving the fault detection rate of software testing and achieving intelligent test code generation, prioritization, and verification, ultimately enhancing the efficiency and effectiveness of software testing.

"""

class Node:
    def __init__(self, title, note=None):
        self.title = title
        self.note = note
        self.children = []
        self.parent = None

    def add_child(self, child):
        child.parent = self
        self.children.append(child)

    def __repr__(self, level=0):
        ret = "\t" * level + repr(self.title) + ("\n" if self.note is None else " " + repr(self.note) + "\n")
        for child in self.children:
            ret += child.__repr__(level + 1)
        return ret

    def get_description(self):
        return f"{self.title}. {self.note}"

def parse_template(template: str):
    '''
    turn a template into tree structure. 
    The template should be in the following format:
        项目摘要（500字以内）
        立项依据
            基本概念及内涵
            需求及科学意义
            研究现状
        项目研究目标、拟解决的关键问题及研究内容（注重成长性、基础性、创新性，注重凝练关键科学技术问题，研究内容应该突出重点）
            研究目标
            拟解决的关键科学问题
            研究内容
            研究周期及阶段划分
        预期研究成果和应用前景
            研究成果，关键指标及考核评估方案（一般采用技术评测，实验验证，第三方测试，潜在应用方评测等方式，开展项目研究成果的验证评估）
                成果1：xxxx
                    考核指标：xxxx
                    评估方案：xxxx
                成果2：xxxx
                    考核指标：xxxx
                    评估方案：xxxx
            
        先进性及应用前景分析（分析项目研究成果和指标的先进性，包括并不限于技术创新水平，技术难度，关键技术指标水平等，并与国内外同类研究情况对比。综合性阐述项目有望产生的效益，经济效益和科学价值，潜在应用方向）
    '''
    lines = template.strip().split('\n')
    root = Node("Root")
    current_node = root
    stack = [root]

    for line in lines:
        line = line.rstrip()
        if not line:
            continue

        # Determine the level by counting leading tabs
        stripped_line = line.lstrip('\t')
        level = len(line) - len(stripped_line)

        # Extract title and note
        if '（' in stripped_line and stripped_line.endswith('）'):
            title, note = stripped_line.split('（', 1)
            note = '（' + note
        else:
            title, note = stripped_line, None

        # Create new node
        new_node = Node(title.strip(), note.strip() if note else None)

        # Adjust stack to the correct level
        while len(stack) > level + 1:
            stack.pop()

        # Add new node to the tree/
        stack[-1].add_child(new_node)

        # Push new node onto stack
        stack.append(new_node)

        # Set current node
        current_node = new_node

    return root

class DetailedReport():
    def __init__(self, query: str, report_type: str, report_source: str, source_urls, config_path: str, websocket: WebSocket, subtopics=[]):
        self.query = query
        self.report_type = report_type
        self.report_source = report_source
        self.source_urls = source_urls
        self.config_path = config_path
        self.websocket = websocket
        self.subtopics = subtopics
        
        # A parent task assistant. Adding research_report as default
        # self.main_task_assistant = GPTResearcher(self.query, "research_report", self.report_source, self.source_urls, self.config_path, self.websocket)
        self.main_task_assistant = GPTResearcher(
            query=self.query, 
            report_type="research_report", 
            report_source=self.report_source, 
            source_urls=self.source_urls, 
            config_path=self.config_path, 
            websocket=self.websocket, 
            context=[task_description])
        self.existing_headers = []
        # This is a global variable to store the entire context accumulated at any point through searching and scraping
        self.global_context = []
    
        # This is a global variable to store the entire url list accumulated at any point through searching and scraping
        if self.source_urls:
            self.global_urls = set(self.source_urls)

    async def run(self):
        # make relavent context, gather relavent urls
        # await self._initial_research()

        # report_introduction = await self.main_task_assistant.write_introduction()

        # Get list of all subtopics
        template = open("./local_knowledge/template.txt", "r").read()
        outline_root = parse_template(template)
        report_body = await self.generate_body(outline_root)

        # Construct the final list of visited urls
        self.main_task_assistant.visited_urls.update(self.global_urls)

        # Construct the final detailed report (Optionally add more details to the report)
        report = await self._construct_detailed_report("", report_body)

        return report

    async def make_draft(self):
        await self.main_task_assistant.make_context()
        self.global_context = self.main_task_assistant.context
        self.global_urls = self.main_task_assistant.visited_urls

    async def _initial_research(self):
        # Conduct research using the main task assistant to gather content for generating subtopics
        await self.main_task_assistant.make_context()
        # Update context of the global context variable
        self.global_context = self.main_task_assistant.context
        # Update url list of the global list variable
        self.global_urls = self.main_task_assistant.visited_urls

    async def _get_all_subtopics(self) -> list:
        '''
        Use llm to generate some sub topics based on query, context and default subtopics (optional)
        '''
        subtopics = await self.main_task_assistant.get_subtopics()
        return subtopics.dict()["subtopics"]

    async def generate_body(self, node: Node, level: int=0):
        if node.title == "Root":
            markdown = ""
        else:
            heading = "#" * level + " " + node.title + "\n"
            body = await self.generate_sections(
                subtopic=node.get_description(), 
                parent_query = self.query,
                subtopics=[])
            markdown = heading + body + "\n"

        for child in node.children:
            markdown += await self.generate_body(child, level + 1)
        return markdown

    async def _generate_subtopic_reports(self, subtopics: list) -> tuple:
        subtopic_reports = []
        subtopics_report_body = ""

        async def fetch_report(subtopic):

            subtopic_report = await self._get_subtopic_report(subtopic)

            return {
                "topic": subtopic,
                "report": subtopic_report
            }

        # This is the asyncio version of the same code below
        # Although this will definitely run faster, the problem
        # lies in avoiding duplicate information.
        # To solve this the headers from previous subtopic reports are extracted
        # and passed to the next subtopic report generation.
        # This is only possible to do sequentially

        # tasks = [fetch_report(subtopic) for subtopic in subtopics]
        # results = await asyncio.gather(*tasks)

        # for result in filter(lambda r: r["report"], results):
        #     subtopic_reports.append(result)
        #     subtopics_report_body += "\n\n\n" + result["report"]

        for subtopic in subtopics:
            result = await fetch_report(subtopic)
            if result["report"]:
                subtopic_reports.append(result)
                subtopics_report_body += "\n\n\n" + result["report"]

        return subtopic_reports, subtopics_report_body

    async def _get_subtopic_report(self, subtopic: dict) -> tuple:
        current_subtopic_task = subtopic.get("task")
        return await self.generate_sections(current_subtopic_task, self.query, self.subtopics)
        
    async def generate_sections(self, subtopic, parent_query, subtopics: dict):
        subtopic_assistant = GPTResearcher(
            query=subtopic,
            report_type="subtopic_report",
            websocket=self.websocket,
            parent_query=parent_query,
            visited_urls=self.global_urls,
            # agent=self.main_task_assistant.agent,
            role=self.main_task_assistant.role
        )

        # The subtopics should start research from the context gathered till now
        subtopic_assistant.context = list(set(self.global_context))

        # Conduct research on the subtopic
        await subtopic_assistant.make_context()

        # Here the headers gathered from previous subtopic reports are passed to the write report function
        # The LLM is later instructed to avoid generating any information relating to these headers as they have already been generated
        subtopic_report = await subtopic_assistant.write_report(self.existing_headers)

        # Update context of the global context variable
        self.global_context = list(set(subtopic_assistant.context))
        # Update url list of the global list variable
        self.global_urls.update(subtopic_assistant.visited_urls)

        # After a subtopic report has been generated then append the headers of the report to existing headers
        self.existing_headers.append(
            {
                "subtopic task": subtopic,
                "headers": extract_headers(subtopic_report),
            }
        )

        return subtopic_report

    async def _construct_detailed_report(self, introduction: str, report_body: str):
        # Generating a table of contents from report headers
        toc = table_of_contents(report_body)
        
        # Concatenating all source urls at the end of the report
        report_with_references = add_source_urls(report_body, self.main_task_assistant.visited_urls)
        
        return f"{introduction}\n\n{toc}\n\n{report_with_references}"